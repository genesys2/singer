# Development of the prototype pages on SINGER for the GRSS - GCP project G8009.05.01 #



## Notes of the meeting - 28/01/11 ##


**Participants**: Claire Billot, JC Glaszmann, JF Rami, Federico Mattei, Luca Matteis, Elizabeth Arnaud

### 1.	Principle: ###

  1. he very first step of GCP work on reference sets  was to develop the composite collections mixing core collection, breeding material, partners accessions. For Sorghum, it was composed by material form ICRISAT, CIRAD, CAAS. Only CGIAR genebank accessions are in SINGER so a Warning should inform when possible about where are the accessions provided by partners.
  1. enebanks have produced a core collection that represent the diversity and then SSr marker were developed. – seeds are available but not purified For SORGHUM, The core collections was published in 2009-

**ACTION: Claire will send the key articles related to the Sorghum core collection**

  1. rom this core , a minicore was extracted (size variable per crop but an average of 200/300 accessions) that are manageable by users
  1. nucleus has been defined with purified seeds (single parent plant)  - It is around 20  accessions and is dense in diversity. This is what we call GENETIC STOCKS .  Nucleus is minicore + additional accessions of interest for the use-Elite material can be added –  Each accession of the  nucleus got a new accession ID  and are kept in a separate collection at ICRISAT at least. A new quality is guaranteed. – ICRISAT distribute these seeds but it may not correspond to the seed lost used in the experioements so needs to be documented.

**ACTION: regarding the GS, Elizabeth will ask Hari if  they give IDs to the GS  and which one?), will ask what are the IG accessions and ICCv**

**Summary:**
  * The CORE sample collection set was developed before 2005 and is made up of 2000/3000 accessions
  * This set was further filtered between 2005 and 2008 in order to create a set of 200/300 accessions denoted as MINICORE sample collection
  * Then the GCP identified a set of Genetic Stock accessions and created a set denoted as CORE Reference Set
  * astly, a NUCLEUS was indentified (less than 50 accessions) which included the accession representing all the genetic subgroups

**All four of these sets (per crop) must be shown on SINGER and a strong suggestion that all of the accessions in the nucleus must be ordered should appear (though it will not be mandatory)**

### 2.	SINGER prototype - For the pages we need to get the possibility to access: ###

  * 1 core collection
  * 1 minicore
  * 1 nucleus - GS – the ordering should propose the germplasm request

**Action: Remove the Germplasm\_ID column.**

The link with Genediversity  (http://gohelle.cirad.fr:8080/GenDiversity/Home
) that contains the genotyping results and I used to natch germplasmId to accessioned was discussed. One accession will then be linked to several genotyping results. 1 accession ID can point to several germplassm IDs in one or several institutes. :

**ACTION: There is a need to develop a central web service to map the ‘AccessionID’ with ‘Germplasm ID’, or a clever URL that embarks the accession ID – Elizabeth will contact Manuel to see of guilhem can help on Genediversity side.**

### 3.	Lists per crops ###
It was decided to continue the matching work on SINGER with:

  1. Chickpea, Sorghum and Musa**have been identified as the priority crops to be used as a pilot
  1. Wheat and Maize** will follow and then all the other crops

Federico must contact Claire Billot for any questions on the list and data: claire.billot@cirad.fr

**ACTION: Claire will send the lists to Frederico starting with Musa in order to get ready for Musanet meeting**


---

# Support service (GRSS) for the plant breeding community #

## Report January 2011 – March 2011 ##

## By Federico Mattei ##

1. Program Description and General Objectives of the Program
2. Specific Objectives of the Terms of Reference
3. Tasks Completed
> - Creating the initial structure
> - Analyzing and preparing the data
> - Uploading the data
4. Tasks Left to Complete
> - Data still missing
5. Conclusions


## 1. Program Description ##

> ### Objectives and outputs ###
A major intention of the GCP for the coming years is to create a set of Plant Breeding Support Services that are both sustainable and public goods. This will facilitate access by plant breeders in the South to modern plant science technologies, at optimal cost and with logistical and technical support. Activities supported by a Integrated Breeding Platform (IBP) will address specific needs identified by the breeders in the National Agricultural Research Systems (NARS). The IBP will include several breeding services which address germplasm, markers and traits. They are all related and complementary, yet each component (service) could be used independently. This means that users apply for one or several services, depending on local needs and resources.

The major objective of this project is to develop one of these components, namely the Genetic Resources Support Service (GRSS).

## 2. Specific Objectives of the Terms of Reference ##

Contribute to the completion of outputs for the GCP projects entitled ‘Establishing a Genetic Resource Support Service (GRSS) to the plant breeding community’. The work contributes to enhance the GCP data related to the Reference Germplasm sets and will also contribute to improve the quality of the Passport data provided to GENESYS
Activities:
1.	Select the priority crops with the Coordinator
2.	Curate SINGER passport data and flag the accessions being part of the GCP reference sets
3.	Collect information and provide feedback on quality by liaising with gene bank’s curators and database managers, and with CIRAD/ICRISAT Collaborators of the GRSS project
4.	Provide the list of accessions with flags to Luca Mattei (Web Developer - Bioversity) for the display on SINGER
5.	Curate the data on collecting missions and collected samples, particularly for those related to the accessions in the GCP reference sets, checking the links to the collecting forms and their metadata

## 3. Tasks Completed ##
### 3.1.1 Creating the Structure ###
A meeting was held on the 28/01/2011 in which Claire Billot, JC Glaszmann, JF Rami, Federico Mattei, Luca Matteis, Elizabeth Arnaud took part. The structure needed was discussed and some background information was provided.
Background Summary:
•	The CORE sample collection set was developed before 2005 and is made up of 2000/3000 accessions
•	This set was further filtered between 2005 and 2008 in order to create a set of 200/300 accessions denoted as MINICORE sample collection
•	Then the GCP identified a set of Genetic Stock accessions and created a set denoted as CORE Reference Set
•	Lastly, a NUCLEUS was indentified (less than 50 accessions) which included the accession representing all the genetic subgroups
It was agreed that all four of these sets (per crop) must be shown on SINGER and a strong suggestion that all of the accessions in the nucleus must be ordered should appear (though it will not be mandatory)

===3.1.2 Selection of Priority Crops
In coordination with the GCPO coordinator, Elizabeth Arnaud, the Priority crops were selected. It was decided that initially that three crops would be given priority. These were:
1.	Chickpea
2.	Sorghum
3.	Musa
Subsequently the following crops would be added
1.	Wheat
2.	Maize
All the other crops (Barley, Coconut, Bean, Cowpea, Foxtail Millet, Finger Millet, Groundnut, Lentil, Pearl Millet, Pigeon Pea and Rice) would be added in the last phase.

### 3.1.3 Defining the structure on SINGER ###
In collaboration with Luca Matteis and Milko Skofic a prototype page on SINGER needed to host and present the data was defined. It was decided that each crop would be associated to three pages.
The initial page would host the list of accessions included in the Core Reference Set and would indicate which of these accessions was also part of the Nucleus.

Another page would host all the accessions included in the Core Sample Collection and would indicate if said accessions were also part of the Minicore.


These accessions would be listed including their Accession Number, Institute Name, Collection Name, Taxonomy and Country Source. It was furthermore decided that these lists would include mechanisms that would enable the user to sort and/or search accessions. Obviously, the possibility to order these accessions through the “Shopping Cart” would be possible.

It was also decided that, on the page hosting the Core Reference Set, when a user ordered a single accession, a message would appear strongly suggesting that the user order the whole set. In order to facilitate this action, a button used to “Select Complete Set” was created and included on the page.

A third and last page, per crop, was defined in order to provide a space for providing the relevant bibliography, uploading different media and for suggesting and replying to comments.


We also implemented a tool with which to download the results in csv format and made sure the columns were sortable


### 3.1.4 SSR Data ###
Due to the fact that one of the principal needs associated with this development     was the desire to show SSR data relative to each accession, it was decided to include an extra column in the accession lists where we could insert a link leading to a CIRAD hosted site displaying the following data for each accession.
-	Accession Number
-	Locus
-	Allele
-	Allele Amount
The possibility of showing the SSR data for several accessions simultaneously was also discussed and then implemented (separating accession number with a comma in URL)



### 3.2.1	Analyzing and preparing the data ###
In early January, we received sixteen (16) excel files from the CIRAD consultant Sarah Katherine Mcgrath, relative to sixteen different crops (Chickpea, Sorghum, Musa, Wheat, Maize, Barley, Coconut, Bean, Cowpea, Foxtail Millet, Finger Millet, Groundnut, Lentil, Pearl Millet, Pigeon Pea and Rice). The excel files included the list of accessions included in the Reference Set for each crop.

Furthermore, for Chickpea, Musa and Sorghum the lists of accessions identified as pertaining to the Core and Minicore Collections were downloaded from the ICRISAT website.
Chickpea Core
http://www.icrisat.org/what-we-do/crops/ChickPea/Project1/Core/Start.htm
Chickpea Minicore
http://test1.icrisat.org/ChickPea/Minicore.htm
Musa Mini Core
http://gcpcr.grinfo.net/index.php?app=datasets&inc=dataset_details&dataset_id=576
Sorghum Core:
http://test1.icrisat.org/sorghum/Project1/Core/Start.htm
Sorghum Minicore
http://test1.icrisat.org/sorghum/Project1/Sorghum_minicore.htm

This table shows the number of accessions per crop and per set that were received or, in the case of Musa, Chickpea and Sorghum, obtained

Furthermore, we managed to obtain the Bibliography for Chickpea (6 articles) from H.D. Upadhyaya of ICRISAT
Bibliography
Upadhyaya HD, Ortiz R (2001) A mini core subset for capturing diversity and promoting utilization of chickpea genetic resources. Theor Appl Genet 102:1292-1298
Upadhyaya HD, Bramel PJ, Singh S (2001) Development of a chickpea core subset using geographic distribution and quantitative traits. Crop Sci 41:206-210
Upadhyaya HD, Furman BJ, Dwivedi SL, Udupa SM, Gowda CLL, Baum M, Crouch JH, Buhariwalla HK, Singh S (2006) Development of a composite collection for mining germplasm possessing allelic variation for beneficial traits in chickpea. Plant Genetic Resources 4:13-19
Upadhyaya HD, Dwivedi SL, Baum M, Varshney RK, Udupa SM, Gowda CLL, Hoisington D, Singh S (2008) Genetic structure, diversity and allelic richness in composite collection and reference set in chickpea (Cicer arietinum L.). BMC Plant Biol 8:106

### 3.3.1 Uploading the Data ###
During the first week of February 2011, Luca Matteis, uploaded the first batch of accessions on to the recently created section in SINGER.
Specifically the following data was uploaded:
Musa: Core Sample Collection, Core Reference Set, Minicore. Furthermore the links relative to the SSR data were included for each uploaded accession
Chickpea: Core Sample Collection, Core Reference Set, Minicore. Furthermore, the bibliography specific to this crop was also uploaded on the relevant comment page.
During the second week of February we are also planning to upload the Core Sample Collection, Core Reference Set and Minicore for Sorghum.

### 4.1.0 Tasks Left to Complete ###
> Although much f the necessary structure has been defined and created, there is still quite of work still to be done relative to the selection and upload of data.
For some crops, notably wheat and maize, no information has been received while for many others, data is still missing. For none of the crops, so far, we have received all the necessary information.
It would be important for every centre, or for every person responsible for each crop, to clearly define the accessions to be included in each set.
If and when Luca Matteis, receives a series of excel files with the sets for each crop clearly defined, it will not be hard for him to upload this data in to the already created structure.
Thus, I would suggest that each centre send the following, well defined, batches of information for each pertinent crop excluding the ones we have already uploaded and which are noted previously in this same document:
-	Core Sample Collection (with an indication relative to which of these accessions are part of the NUCLEUS)
-	Core Reference Set (with an indication relative to which of these accessions are part of the MINICORE)
-	Bibliography
-	Links to SSR data per accession
Once Luca Matteis, receives said data he will then upload them on to the relevant page on SINGER.

### 5.1.0 Conclusions ###
In conclusion, though much work has been done on the organizational aspects of this output and though the structure has been defined and created, much work still has to be done on the selection and upload of data.
In my opinion, it would be a good idea to use the three crops which are most complete (Musa, Chickpea and Sorghum) in order to showcase the tool with the centers’ Genebank Curators and Database Managers and entice them to send the necessary information for all the other crops. Once Luca Matteis, receives said information, he will be able to upload it with ease.
Furthermore the tool should be publicized with the intended users in order to receive their feedback on how to ameliorate the tool and take in to consideration other suggestions they might have.
Lastly, a communication strategy should be defined as it is needed to divulge this new tool and to get users to start using said tool (including the comment and reply section)