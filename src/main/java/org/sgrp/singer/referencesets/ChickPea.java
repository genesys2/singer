package org.sgrp.singer.referencesets;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import java.util.ArrayList; 
import java.util.Map; 
import java.util.HashMap; 

// this has some DB api
import org.sgrp.singer.AccessionServlet;

import org.sgrp.singer.SqlDataInterface;

/**
 * It's just a data container for our SQL query. 
 * Implements the SqlData interface
 */
public class ChickPea implements SqlDataInterface {


    public ChickPea() {

    }

    public String getSql() {
        String sql =  "select *, accdata.`accenumb` as anumb from chickpea "
            +"left join accdata "
            +"on accdata.accenumb = REPLACE(chickpea.`ACCENUMB`, 'ICC', '') "
            +"where accdata.`taxname` = 'Cicer arietinum' "
            +"union "
            +"select *, accdata.`accenumb` as anumb from chickpea "
            +"left join accdata "
            +"on accdata.accenumb = CONCAT('ig ', REPLACE(chickpea.`ACCENUMB`, 'IG', '')) "
            +"where accdata.`taxname` = 'Cicer arietinum'";
        return sql;
    }

    public int getItemsPerPage() {
        return 30;
    }
    public int getNumberOfPages() {
        return 10;
    }


    
}
