package org.sgrp.singer.scaffolding;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;

import org.sgrp.singer.AccessionServlet;
import org.sgrp.singer.referencesets.ChickPea;

public class ScaffoldingTest extends TestCase {
    static {
        AccessionServlet.loadInitData("/Users/lucamatteis/bioversity-singer/trunk/src/main/webapp/WEB-INF/singer.properties");
    }

    public void testMain() throws SQLException {
        Connection conn = AccessionServlet.openConnection();

        try {
            // create a SqlDataInterface object (ChickPea implements it)
            ChickPea cp = new ChickPea(); 
            // and pass it to the Scaffolding constructor
            String pageNum = "2";
            String orderColumn = "id";
            Scaffolding s = new Scaffolding(cp, conn, pageNum, orderColumn, "");

            ResultSet currPage = s.getCurrentPage();
        } catch (Exception e) {
            e.printStackTrace();
        }

        conn.close();
    }

    public void testBadPageNumber() throws SQLException {
        Connection conn = AccessionServlet.openConnection();

        try {
            // create a SqlDataInterface object (ChickPea implements it)
            ChickPea cp = new ChickPea(); 
            // and pass it to the Scaffolding constructor
            String pageNum = "-22";
            String orderColumn = "id";
            Scaffolding s = new Scaffolding(cp, conn, pageNum, orderColumn, "");
            ResultSet currPage = s.getCurrentPage();
        } catch (Exception e) {
            e.printStackTrace();
        }

        conn.close();
    }
    public void testPagination() throws SQLException {
        Connection conn = AccessionServlet.openConnection();

        try {
            // create a SqlDataInterface object (ChickPea implements it)
            ChickPea cp = new ChickPea(); 
            // and pass it to the Scaffolding constructor
            String pageNum = "9";
            String orderColumn = "id";
            Scaffolding s = new Scaffolding(cp, conn, pageNum, orderColumn, "");

            System.out.println(s.getPagination());
        } catch (Exception e) {
            e.printStackTrace();
        }

        conn.close();
    }

}
